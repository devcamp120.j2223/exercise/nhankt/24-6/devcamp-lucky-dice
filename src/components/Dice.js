import { Component } from "react";
import imgDefault from "../assets/images/dice.png"
import img1 from "../assets/images/1.png"
import img2 from "../assets/images/2.png"
import img3 from "../assets/images/3.png"
import img4 from "../assets/images/4.png"
import img5 from "../assets/images/5.png"
import img6 from "../assets/images/6.png"


class Dice extends Component{
    constructor(props){
        super(props)
        this.state = {image : imgDefault}
    }
    handleClick =()=>{
        let index = Math.floor(Math.random() * 6);
        switch (index) {
            case 0:
                this.setState({image : img1})
                break;
            case 1:
                this.setState({image : img2})
                break;
            case 2:
                this.setState({image : img3})
                break;
            case 3:
                this.setState({image : img4})
                break;
            case 4:
                this.setState({image : img5})
                break;
            case 5:
                this.setState({image : img6})
                break;
        }
    }
    render(){
        return(
            <div>
                <div>
                <img height="350px" src={this.state.image}></img>
                </div>
                <button onClick={this.handleClick} style={{width:"100px"}}>Roll</button>
            </div>
        )
    }
}
export default Dice